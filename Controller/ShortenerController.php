<?php


namespace Plapinski\ShortenerBundle\Controller;

use Plapinski\ShortenerBundle\Domain\Service\ShortenerService;
use Plapinski\ShortenerBundle\Application\UseCases\Shorten\Request as ShortenRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ShortenerController
{
    private $shortenerService;

    public function __construct(ShortenerService $shortenerService)
    {
        $this->shortenerService = $shortenerService;
    }

    public function shortenAction(Request $request)
    {
        if('json' !== $request->getContentType()) {
            throw new HttpException(415, 'application/json is the only supported method');
        }

        try {
            $content = json_decode($request->getContent());
            $shortenerResponse = $this->shortenerService->shorten(new ShortenRequest($content->link));

            return new JsonResponse($shortenerResponse);

        } catch (\Throwable $e) {
            throw new HttpException(500);
        }
    }
}