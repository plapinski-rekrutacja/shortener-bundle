<?php


namespace Plapinski\ShortenerBundle\Domain\Service;


use Plapinski\ShortenerBundle\Application\UseCases\Shorten\Request as ShortenRequest;
use Plapinski\ShortenerBundle\Application\UseCases\Shorten\Response as ShortenResponse;

class ShortenerService
{
    const CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const BASE = 62;

    public function shorten(ShortenRequest $request): ShortenResponse
    {
        $link = $request->getLink();
        return new ShortenResponse($link, $this->execute($link));
    }

    private function execute(string $link): string
    {
        $ret = '';

        $chunks = $this->extractMd5Chunks($link);
        $sum = 0;
        foreach($chunks as $chunk) {
            $sum += $chunk;
        }

        foreach($chunks as $chunk) {
            $ret .= $this->base62encode($chunk);
        }

        return $ret;
    }

    private function extractMd5Chunks(string $link): array
    {
        $hash = md5($link, true);
        return unpack('L*', $hash);
    }

    private function base62encode(int $number): string
    {
        $ret = '';

        while($number > 0) {
            $ret .= self::CHARS[$number % self::BASE];
            $number = floor($number / self::BASE);
        }

        return strrev($ret);
    }
}