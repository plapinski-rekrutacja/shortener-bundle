<?php


namespace Plapinski\ShortenerBundle\Application\UseCases\Shorten;


class Response implements \JsonSerializable
{
    private $original;
    private $shortened;

    public function __construct(string $original, string $shortened)
    {
        $this->original = $original;
        $this->shortened = $shortened;
    }

    public function getOriginal(): string
    {
        return $this->original;
    }

    public function getShortened(): string
    {
        return $this->shortened;
    }

    public function jsonSerialize(): array
    {
        return [
            'original' => $this->original,
            'shortened' => $this->shortened
        ];
    }
}