<?php


namespace Plapinski\ShortenerBundle\Application\UseCases\Shorten;


class Request
{
    private $link;

    public function __construct(string $link)
    {
        $this->link = $link;
    }

    public function getLink(): string
    {
        return $this->link;
    }
}