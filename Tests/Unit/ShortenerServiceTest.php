<?php

namespace Plapinski\ShortenerBundle\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Plapinski\ShortenerBundle\Application\UseCases\Shorten\Request;
use Plapinski\ShortenerBundle\Domain\Service\ShortenerService;

class ShortenerServiceTest extends TestCase
{
    private $shortener;

    public function setUp()
    {
        $this->shortener = new ShortenerService();
    }

    public function singleLinkProvider()
    {
        return [
            ['/ludzie/m__b/'],
            ['/link/3765227/zostawil-gazik-w-sercu-pacjenta-sadblad-usprawiedliwiony/'],
            ['/2010/11/simple-url-shortening-algorithm-in-java.html'],
            ['/questions/480230/convert-md5-to-base62-for-url']
        ];
    }

    public function linksArrayProvider()
    {
        return [
            [
                '/ludzie/m__b/',
                '/link/3765227/zostawil-gazik-w-sercu-pacjenta-sadblad-usprawiedliwiony/',
                '/2010/11/simple-url-shortening-algorithm-in-java.html',
                '/questions/480230/convert-md5-to-base62-for-url'
            ],
        ];
    }

    /**
     * @dataProvider singleLinkProvider
     */
    public function test_it_returns_the_same_output_for_the_same_input($link)
    {
        $request = new Request($link);

        $firstResponse = $this->shortener->shorten($request);
        $secondResponse = $this->shortener->shorten($request);

        $this->assertEquals($firstResponse->getShortened(), $secondResponse->getShortened());
    }

    /**
     * @dataProvider linksArrayProvider
     */
    public function test_it_returns_different_results_upon_different_input($firstLink, $secondLink, $thirdLink)
    {
        $firstResult = $this->shortener->shorten(new Request($firstLink));
        $secondResult = $this->shortener->shorten(new Request($secondLink));
        $thirdResult = $this->shortener->shorten(new Request($thirdLink));

        $this->assertNotEquals($firstResult, $secondResult);
        $this->assertNotEquals($firstResult, $thirdResult);
        $this->assertNotEquals($secondResult, $thirdResult);
    }
}
