<?php


namespace Plapinski\ShortenerBundle\Tests\Functional;


use Plapinski\ShortenerBundle\Application\UseCases\Shorten\Request;
use Plapinski\ShortenerBundle\Domain\Service\ShortenerService;
use Plapinski\ShortenerBundle\Tests\Functional\app\AppKernel;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ShortenerControllerTest extends WebTestCase
{
    public function setUp()
    {
        parent::setUp();
        $_SERVER['KERNEL_DIR'] = __DIR__ . '/app/';
        $_SERVER['KERNEL_CLASS'] = AppKernel::class;
    }

    public function test_shortener_service()
    {
        $link = '/link/3765227/zostawil-gazik-w-sercu-pacjenta-sadblad-usprawiedliwiony/';

        $requestData = json_encode([
            'link' => $link
        ]);

        $shortener = new ShortenerService();

        $shortenedLink = $shortener->shorten(new Request($link));

        $expectedResult = json_encode($shortenedLink, true);

        $client = static::createClient();
        $client->request(
            'post',
            '/shorten',
            [],
            [],
            ['HTTP_ACCEPT' => 'application/json', 'CONTENT_TYPE' => 'application/json'],
            $requestData
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(
            'application/json',
            $client->getResponse()->headers->get('content-type')
        );
        $this->assertEquals(
            json_decode($expectedResult, true),
            json_decode($client->getResponse()->getContent(), true)
        );
    }
}