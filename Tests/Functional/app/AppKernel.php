<?php


namespace Plapinski\ShortenerBundle\Tests\Functional\app;

use Plapinski\ShortenerBundle\ShortenerBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel as SymfonyKernel;

class AppKernel extends SymfonyKernel
{
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/config/config.yml');
    }

    public function registerBundles()
    {
        return [
            new FrameworkBundle(),
            new ShortenerBundle()
        ];
    }
}