# Instalacja:

w `composer.json` dodajemy repozytorium i paczkę:

```json
"repositories": [
        {
            "type": "vcs",
            "url": "https://pawellapinski@bitbucket.org/plapinski-rekrutacja/shortener-bundle.git"
        }
    ],
    
    (...)
    
"require": {
    (...)
    "plapinski/url-shortener-bundle": "dev-master"
},
```

W `AppKernel.php` aktywujemy bundle:

```php
    public function registerBundles()
    {
        $bundles = [
            (...)
            new \Plapinski\ShortenerBundle\ShortenerBundle()
        ];

        (...)
        
        return $bundles;
    }
```

W `app/config/routing.yml` dodajemy routing:

```yaml
shortener:
    resource: '@ShortenerBundle/Resources/config/routing.yml'
```

Bundle w akcji możemy ujrzeć posiłkując się np. `curl`-em wykonując:

```
curl -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"link":"\/ludzie\/m__b/"}' symfony.local/shorten
```

co da nam zwrotkę w postaci:
```json
{
  "original": "/ludzie/m__b/",
  "shortened":"1D1sCG1plNOb4Mrx6V10emZe"
}
```
